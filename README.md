# Video4Linux Untuk Mengatur Output Loopback Video Devices
#### Kelompok Broprog | Sysprog-C

### Anggota:
    1. Astrida Nayla Fauzia (1806235826)
    2. Muhammad Jilham Luthfi (1806141340)
    3. Talitha Luthfiyah Dhany Maheswari (1806186603)

### Tentang Project
Loopback video device merupakan virtual device yang dapat berfungsi layaknya media device pada umumnya dan digunakan oleh komputer untuk berinteraksi dengan dirinya sendiri. Device yang akan dikontrol oleh modul v4l2loopback ini adalah video device berupa webcam pada komputer. 

Kelompok kami akan mengontrol parameter pada module v4l2loopback agar bisa membuat sebuah loopback video device baru yang dapat dideteksi oleh aplikasi normal seperti Google Meet. Aplikasi normal akan membaca loopback video device seakan-akan ia merupakan video device biasa. 

Pengaturan modul v4l2loopback ini akan kami implementasikan sedemikian mungkin sehingga pengguna dapat memilih apakah output dari webcamnya merupakan screen layarnya sendiri atau sebuah static image yang ia tentukan sendiri.

### Panduan Penggunaan
Untuk mensimulasikan kegunaan device driver v4l2loopback dan mengontrol output loopback video device, jalankan script main_script.sh
```
$ bash main_script.sh
```

### Panduan Untuk Script midtier2.sh
    1. Jalankan menu 1, 2, 3, 4, dan 5 secara berurutan.
    2. Apabila sudah pernah menjalankan menu 1 sebelumnya, dapat langsung menjalankan menu lainnya. Namun, pastikan anda menjalankan menu 4 dulu sebelum menu 5.
    3. Apabila ingin menghentikan eksekusi loopback video device, tekan Ctrl-C di keyboard anda.
    4. Apabila ingin menghentikan eksekusi script, pilih menu 6 pada main menu.