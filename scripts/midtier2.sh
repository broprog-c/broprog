#!/bin/bash

main_menu() {
    read -p "
    ========================================
                   MAIN MENU
    ========================================
    1. Setup & Install Dependencies
    2. Activate Loopback Device for Webcam Output
    3. Set Image as Webcam Output
    4. Exit
    Choose 1-4: " menu_option

    if [ $menu_option == 1 ]; then
        setup
    elif [ $menu_option == 2 ]; then
        activate_loopback
    elif [ $menu_option == 3 ]; then
        set_image
    elif [ $menu_option == 4 ]; then
        echo "Bye Bye...."
        echo "Press [Enter] key to continue..."
        while [ true ] ; do
            read -s -N 1 -t 1 key
            if [[ $key == $'\x0a' ]] ; then
                exit 1
            fi
        done
    else
        echo "Invalid input, try again!"
        main_menu
    fi
}

setup() {
    echo "-------------------------------------------------------"
    echo "            Setup and Install Dependencies             "
    echo "-------------------------------------------------------"
    
    echo "---- Initiate Setup for v4l2loopback"
    sudo apt-get update
    sudo apt-get remove v4l2loopback-dkms
    sudo apt-get install make build-essential libelf-dev linux-headers-$(uname -r) unzip
    wget https://github.com/umlaeute/v4l2loopback/archive/master.zip
    unzip master.zip
    rm -rf master.zip

    echo "---- Installing Dependencies"
    sudo apt update
    sudo apt install v4l-utils ffmpeg

    echo "Press [Enter] key to continue..."
    while [ true ] ; do
        read -s -N 1 -t 1 key
        if [[ $key == $'\x0a' ]] ; then
            main_menu
        fi
    done
}

activate_loopback() {
    echo "-------------------------------------------------------"
    echo "      Activate Loopback Device for Webcam Output       "
    echo "-------------------------------------------------------"

    cd v4l2loopback-master
    make
    sudo make install

    sudo depmod -a
    sudo modprobe v4l2loopback

    sudo modprobe v4l2loopback exclusive_caps=1

    resolution=`xdpyinfo | awk '/dimensions/{print $2}'`
    first_res=`echo $resolution | awk -Fx '{print $1}'`
    second_res=`echo $resolution | awk -Fx '{print $2}'`

    ((first_res=first_res-1))
    ((second_res=second_res-1))

    new_res=$first_res
    new_res+="x"
    new_res+=$second_res

    ffmpeg -f x11grab -r 15 -s $new_res -i :0.0+0,0 -codec:v rawvideo -pix_fmt yuv420p -threads 0 -f v4l2 /dev/video0

    echo "Loopback device activated! Visit Google Meets to test it!"

    echo "Press [Enter] key to continue..."
    while [ true ] ; do
        read -s -N 1 -t 1 key
        if [[ $key == $'\x0a' ]] ; then
            main_menu
        fi
    done
}

set_image() {
    echo "-------------------------------------------------------"
    echo "              Set Image as Webcam Output               "
    echo "-------------------------------------------------------"

    read -p "Enter the image file name : " filename

    ffmpeg -loop 1 -re -i $filename -f v4l2 -codec:v rawvideo -pix_fmt yuv420p /dev/video0

    echo "Image successfully set as your webcam output! Visit Google Meets to test it!"

    echo "Press [Enter] key to continue..."
    while [ true ] ; do
        read -s -N 1 -t 1 key
        if [[ $key == $'\x0a' ]] ; then
            main_menu
        fi
    done
}

main_menu
