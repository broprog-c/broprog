from django.shortcuts import render
from .forms import InputForm
from subprocess import call

# Create your views here.
def index(request):
    context = {
        'form': InputForm,
    }
    param1 = request.GET.get('option', '')
    if param1 != '':
        handle_request(request)
        return render(request, 'index.html', context)
    else:
        return render(request, 'index.html', context)

def handle_request(request):
    option = request.GET.get('option')
    if option == '1':
        command = '../scripts/midtier2_args.sh 1'
        with open(command, 'rb') as file:
            script = file.read()
        rc = call(script, shell=True)
    elif option == '2':
        rate = request.GET.get('rate')
        output = request.GET.get('output')
        command = '../scripts/midtier2_args.sh 2 ' + rate + ' ' + output
        with open(command, 'rb') as file:
            script = file.read()
        rc = call(script, shell=True)
    elif option == '3':
        filename = request.GET.get('filename')
        command = '../scripts/midtier2_args.sh 3 ' + filename
        with open(command, 'rb') as file:
            script = file.read()
        rc = call(script, shell=True)
    else:
        print('The input is invalid!')
